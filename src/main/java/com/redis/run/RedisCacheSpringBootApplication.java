package com.redis.run;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableCaching
@ComponentScan("com")
@EnableJpaRepositories("com.redis.repository")
@EntityScan("com.redis.entity")
public class RedisCacheSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisCacheSpringBootApplication.class, args);
    }

}
